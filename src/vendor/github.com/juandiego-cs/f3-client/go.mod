module github.com/juandiego-cs/f3-client

go 1.16

require (
	github.com/juandiego-cs/f3-classes v1.0.6
	github.com/google/uuid v1.2.0
)
