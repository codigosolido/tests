package classes

type Account struct {
	Country                 string                `json:"country"`
	BaseCurrency            string                `json:"base_currency"`
	BankId                  string                `json:"bank_id"`
	BankIdCode              string                `json:"bank_id_code"`
	AccountNumber           string                `json:"account_number"`
	Bic                     string                `json:"bic"`
	Iban                    string                `json:"iban"`
	CustomerId              string                `json:"customer_id"`
	Name                    [4]string             `json:"name"`
	AlternativeNames        [3]string             `json:"alternative_names"`
	AccountClassification   AccountClassification `json:"account_classification"`
	JointAccount            bool                  `json:"joint_account"`
	AccountMatchingOptOut   bool                  `json:"account_matching_opt_out"`
	SecondaryIdentification string                `json:"secondary_identification"`
	Switched                bool                  `json:"switched"`
	// PrivateIdentification      PrivateIdentification      `json:"private_identification"`
	// OrganisationIdentification OrganisationIdentification `json:"organisation_identification"`
	// Relationships              Relationships              `json:"relationships"`
}
