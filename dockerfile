FROM golang
WORKDIR /app

# Copy Files
COPY ./src /app
COPY ./data /data

# Get Dependencies
RUN go install -v ./...
RUN go build

# Perform Tests
CMD CGO_ENABLED=0 go test -v ./...