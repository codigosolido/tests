# Form3 Take Home Excersice Test

This repository is part of the project [Form3](https://bitbucket.org/codigosolido/workspace/projects/FOR) as technical exercise made by Juan Diego Munoz for Form3, as part of the enrolment proccess at [Form3](https://github.com/form3tech-oss/interview-accountapi/blob/master/README.md).

This is the very first in my life I get in contact with go, and I must admit Iḿ fascinating. The same decoupling I experienced years ago creating modules for my programs comes again with the division of projects and how easy is import them.

The idea of creating three different repositories is to make them as much reusable as possible. One project may need to use the classes without interacting with the client.

The project is intended to test the [client library](https://github.com/juandiego-cs/f3-client/src/) written in go implementing the required functionality to create, fetch and delete accounts using the [Form 3 API](https://api-docs.form3.tech/api.html).

## Structure

In the **root directory**, we can find a dockerfile file to build the test project into a docker image for further use in the docker-compose structure. The docker-compose.yml file prepare three services needed to run the tests: The database service, The api service and The vault service, then build and run the tests from its dockerimage.

The **data directory** contains the dataset for the tests, and one file to initialise the postgres database. We may modify the set of tests just by editing the file testdata.json in that directory. It is an array of test cases with the structure

- name : name of the test
- input : input accout for creation
- wantErr : boolean value indicating whether we expect an error in this test or not.

The **src directory** contains the source for the test. A main dummy file to compile the project and the main_test.go file where the tests are implemented. As this is a private repository, in order to make availabe the dependencies for the project in the docker file, I've executed the command

> go mod vendor

inside the src folder to download the dependencies to the **vendor** folder. This is a workaround to make the project run with a single docker-compose command. We could remove them by using the start cmd, which execute the download. Including dependencies in the project should be avoided.

## Run

You can run the test in two different ways

### Standalone

If you want to run the test several times or probably changing the entry data, you can do it in this standalone way. You will need to run the associated services for the test an then execute the test commands.

> docker-compose up -d

This command will start the services. Test included, but will leave database, vault and api running.

> . ./src/setenv 

Test takes the values of the api host from [environment variables](#environment-variables). There is a bash script file to set up the environment variables. to the default values in /src/setenv

> cd src \
> go test -v

We can go then to our src directory, where the test project code resides and execute the test there. You can show the output with the flag -v.

### Docker Compose

A much more symple way of running the test is just execute the prepared docker-compose file. This may be more suitable in a CI/CD pipeline.

> docker-compose up --exit-code-from test

This will prepare the services, execute the tests and exit after the execution.

## Environment Variables

### F3_API_SCHEMA 
The tcp schema to communicate with the API. Usually http or https
### F3_API_HOST
The host where the API is exposed. If we are using the services started in the previous step, it should be localhost:8080
### F3_API_CLIENTID
A uuid to identify the client who is creating the accounts,like  634e3a41-26b8-49f9-a23d-26fa92061f38