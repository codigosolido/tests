package main_test

import (
	"encoding/json"
	"io/ioutil"
	"net/url"
	"os"
	"reflect"
	"testing"

	"github.com/juandiego-cs/f3-classes"
	"github.com/juandiego-cs/f3-client"
)

// Input data
type test struct {
	Name    string          `json:"name"`
	Input   classes.Account `json:"input"`
	Output  classes.Message `json:"output"`
	WantErr bool            `json:"wantErr"`
}

// Client to perform the tests
var c client.Client = client.Client{
	BaseUrl:        url.URL{Scheme: os.Getenv("F3_API_SCHEMA"), Host: os.Getenv("F3_API_HOST")},
	OrganisationId: os.Getenv("F3_API_CLIENTID"),
}

// Input test data
var tests []test

// Load input data
func TestLoadData(t *testing.T) {
	file, err := ioutil.ReadFile("../data/testdata.json")
	if err != nil {
		t.Errorf("Missing input data. %v", err)
	}
	err = json.Unmarshal([]byte(file), &tests)
	if err != nil {
		t.Errorf("Malformed input data. %v", err)
	}
}

// Test create
func TestClient_Create(t *testing.T) {
	for i, tt := range tests {
		t.Run(tt.Name, func(t *testing.T) {
			got, err := c.Create(tt.Input)
			if (err != nil) != tt.WantErr {
				t.Errorf("Client.Create() error = %v, wantErr %v", err, tt.WantErr)
			}
			tests[i].Output = *got
		})
	}
}

// Test fetch
func TestClient_Fetch(t *testing.T) {
	for _, tt := range tests {
		t.Run(tt.Name, func(t *testing.T) {
			got, err := c.Fetch(tt.Output.Data.Id)
			if (err != nil) != tt.WantErr {
				t.Errorf("Client.Fetch() error = %v, wantErr %v", err, got)
			}
			if !reflect.DeepEqual(got.Data, tt.Output.Data) {
				t.Errorf("Client.Create() = %v, want %v", got, tt.Output.Data)
			}
		})
	}
}

// Test Delete
func TestClient_Delete(t *testing.T) {
	for _, tt := range tests {
		t.Run(tt.Name, func(t *testing.T) {
			got, err := c.Delete(tt.Output.Data.Id, tt.Output.Data.Version)
			if (err != nil) != tt.WantErr {
				t.Errorf("Client.Delete() error = %v", err)
			}
			if !reflect.DeepEqual(got.Data, tt.Output.Data) {
				t.Errorf("Client.Delete() = %v, want %v", got, tt.Output.Data)
			}
		})
	}
}
