package classes

type OrganisationIdentification struct {
	Identification string `json:"identification"`
	Address        string `json:"address"`
	City           string `json:"city"`
	Country        string `json:"country"`
	Actor          Actor  `json:"actor"`
}
