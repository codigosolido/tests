module github.com/juandiego-cs/f3-tests

go 1.16

require (
	github.com/juandiego-cs/f3-classes v1.0.6
	github.com/juandiego-cs/f3-client v1.0.8
)
