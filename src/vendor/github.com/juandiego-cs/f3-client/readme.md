# Form3 Take Home Excersice Client Library

This repository is part of the project [Form3](https://bitbucket.org/codigosolido/workspace/projects/FOR) as technical exercise made by Juan Diego Munoz for Form3, as part of the enrolment proccess at [Form3](https://github.com/form3tech-oss/interview-accountapi/blob/master/README.md).

This library written in Go implements the `Create`, `Fetch`, and `Delete` operations on the `accounts` resource of the [From3 API](http://api-docs.form3.tech/api.html#organisation-accounts).

## Structure

The project 
- client.go: Main file of the library. It defines the field, the constructor and the operations over the API.
- http.go: File with functions to operate http requests.
- client_test.go: File to perform the tests over the library.


## Usage

To Include the library in your project, run the command

> go get bitbucket.org/codigosolido/client

### Initialise the Client

Then you can start using the client in your .go file. To initialize the client, you must provide the API url and the client id under the operations will be performace by. Eg.

```go
    var c client.Client = client.Client{
        BaseUrl:        url.URL{Scheme: "http", Host: "api.form3.com"},
        OrganisationId: "uuid-with-your-client-id",
    }
```

The client implements the functions of **create**, **fetch** and **delete** accounts calling to the API.

### Create Account

Takes an account input of the type **classes.Account**, and return a **classes.Message** with the new created account in the **Data.Attributes** field. Eventualy, in case of failure, it will return an error. Eg.

```go
    	var input = classes.Account{
        Country: "ES",
            BaseCurrency: "EUR",
            AccountClassification: classes.Business,
            Name: [4]string{
                "Codigo Solido",
                "Empty",
                "Empty",
                "Empty"},
            AlternativeNames: [3]string{
                "CS",
                "Empty",
                "Empty",
			},
    }
    got, err := c.Create(input)
    if err != nil {
        fmt("Something went wrong : %v", err)
    }
```

### Fetch Account

Retrieves an account given its id. It Returns a **classes.Message** with the resource in the **Data** field. Eventually an error. Eg.

```go
    got, err := c.Fetch("2f39c81d-ee08-4038-b507-3f4f9cec1de2")
    if err != nil {
        fmt("Something went wrong : %v", err)
    }
```


### Delete Account

Deletes an account given its id. It Returns a messsage of type **classes.Message** with the deleted account. It performs a fech of the account before deletion. Eventually returns an error. Eg.

```go
    got, err := c.Delete("2f39c81d-ee08-4038-b507-3f4f9cec1de2", "2")
    if err != nil {
        fmt("Something went wrong : %v", err)
    }
```

## Dependencies

	bitbucket.org/codigosolido/classes v1.0.5
	github.com/google/uuid v1.2.0
    
## Tests

Test are stored in different package (client_test) to avoid importing them when the client package is referenced.