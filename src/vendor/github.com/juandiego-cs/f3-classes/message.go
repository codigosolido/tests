package classes

type Message struct {
	Data  Resource `json:"data"`
	Links Links    `json:"links"`
}
