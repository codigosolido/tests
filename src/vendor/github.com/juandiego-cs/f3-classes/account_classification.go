package classes

type AccountClassification string

const (
	Personal  AccountClassification = "Personal"
	Bussiness AccountClassification = "Bussiness"
)

// String - Creating common behavior - give the type a String function
func (d AccountClassification) String() string {
	return string(d)
}

// EnumIndex - Creating common behavior - give the type a EnumIndex function
func (d AccountClassification) EnumIndex() int {
	switch d {
	case Personal:
		return 0
	case Bussiness:
		return 1
	}
	return -1
}
